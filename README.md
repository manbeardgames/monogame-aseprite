# Project has been moved
This project has been moved to GitHub.  This GitLab repository will be removed once the NuGet package has been updated to reflect new repository location.

You can find the GitHub repository at https://github.com/manbeardgames/monogame-aseprite